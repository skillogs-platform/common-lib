package router

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/skillogs-platform/common-lib/xhttp/security"
)

type AuthStub struct {
	verify    func(next http.Handler) http.Handler
	authorize func(next http.Handler, roles ...security.Role) http.Handler
}

func (a *AuthStub) Verify(next http.Handler) http.Handler {
	return a.verify(next)
}

func (a *AuthStub) Authorize(next http.Handler, roles ...security.Role) http.Handler {
	return a.authorize(next, roles...)
}

type CtrlStub struct {
	routes Routes
}

func (c *CtrlStub) Routes() Routes {
	return c.routes
}

func TestNewRouter(t *testing.T) {
	auth := &AuthStub{}
	r := NewRouter(auth)
	assert.NotNil(t, r)
}

func TestRouter_HandleRoutables(t *testing.T) {
	auth := &AuthStub{}
	ctrl := &CtrlStub{routes: Routes{{Name: "route", Method: "GET", Pattern: "/route", HandlerFunc: MyHandler}}}
	routes := Routables{
		"/ctrl": ctrl,
	}
	r := NewRouter(auth)
	r.HandleRoutables(routes)
	assert.Equal(t, "route", r.Router.GetRoute("route").GetName())
}
