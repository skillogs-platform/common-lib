package router

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/skillogs-platform/common-lib/xhttp"
	"gitlab.com/skillogs-platform/common-lib/xhttp/security"
)

// Auth allow to check if a request contains authentication's informations and if they are valid
type Auth interface {
	// Verify if a request has informations to authenticate user
	Verify(next http.Handler) http.Handler
	// Authorize check if connected user had right role
	Authorize(next http.Handler, roles ...security.Role) http.Handler
}

// Router is an http.Handler that dispatch request to dedicated http.Handler and check for authentication
type Router struct {
	*mux.Router
	Auth
}

// NewRouter create a Router with default configuration
func NewRouter(a Auth) *Router {
	r := mux.NewRouter().StrictSlash(false)
	r.Methods(http.MethodOptions)

	r.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		xhttp.JSONResponse(w, 404, xhttp.Error("router", fmt.Errorf("not found: %s", r.RequestURI)))
	})
	r.MethodNotAllowedHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		xhttp.JSONResponse(w, 405, xhttp.Error("router", fmt.Errorf("method not allowed: %s on %s", r.Method, r.RequestURI)))
	})
	return &Router{r, a}
}

// Routable returns Routes
type Routable interface {
	Routes() Routes
}

// Routables is a multiple Routable index by string
// It allow to register multiple Routable this way :
//  Routables{
//      "route1": Routable1,
//      "route2": Routable2,
//      // ...
//  }
type Routables map[string]Routable

// HandleRoutables add multiple Routable to Router
func (r *Router) HandleRoutables(routables Routables) {
	for path, routable := range routables {
		r.HandleRoutable(path, routable)
	}
}

// HandleRoutable add auth middleware if routable needs to
func (r *Router) HandleRoutable(path string, routable Routable) {
	for _, route := range routable.Routes() {
		var handler http.Handler = route.HandlerFunc
		if len(route.Roles) > 0 {
			handler = r.Authorize(handler, route.Roles...)
		}
		if route.AuthRequire {
			handler = r.Verify(handler)
		}
		logrus.WithFields(logrus.Fields{
			"method": route.Method,
			"path":   path + route.Pattern,
			"name":   route.Name,
			"auth":   route.AuthRequire,
			"roles":  route.Roles,
		}).Debug("Create route")
		rt := r.Methods(route.Method).Path(path + route.Pattern).Name(route.Name)
		if route.Params != nil {
			for k, v := range route.Params {
				rt = rt.Queries(k, v)
			}
		}
		rt.Handler(handler)
	}
}
