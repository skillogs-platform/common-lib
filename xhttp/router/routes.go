package router

import (
	"net/http"
	"reflect"
	"runtime"
	"strings"

	"gitlab.com/skillogs-platform/common-lib/xhttp/security"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	Params      map[string]string
	AuthRequire bool
	Roles       []security.Role
	HandlerFunc http.HandlerFunc
}

type Routes []Route

func NewRoute(method, pattern string, handler http.HandlerFunc) Route {
	return Route{
		Method:      method,
		Pattern:     pattern,
		HandlerFunc: handler,
		Name:        getFunctionName(handler),
		Roles:       make([]security.Role, 0),
	}
}

func getFunctionName(i interface{}) string {
	name := runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
	p := strings.Split(name, ".")
	name = p[len(p)-1]
	return strings.Replace(name, "-fm", "", 1)
}

func (r Route) WithAuthRequire(require bool) Route {
	r.AuthRequire = require
	if require {
		r.Roles = []security.Role{
			security.RoleAdmin,
			security.RoleTrainingEngineer,
			security.RoleTeacher,
			security.RoleStudent,
			security.RoleTutor,
		}
	} else {
		r.Roles = nil
	}
	return r
}

func (r Route) WithParams(params map[string]string) Route {
	r.Params = params
	return r
}

func (r Route) WithRoles(roles ...security.Role) Route {
	r.AuthRequire = len(roles) > 0
	r.Roles = roles
	return r
}

func (rs Routes) WithAuthRequire(require bool) Routes {
	for i := range rs {
		rs[i].AuthRequire = require
		if require {
			rs[i].Roles = []security.Role{
				security.RoleAdmin,
				security.RoleTrainingEngineer,
				security.RoleTeacher,
				security.RoleStudent,
				security.RoleTutor,
			}
		} else {
			rs[i].Roles = nil
		}
	}
	return rs
}

func (rs Routes) WithRoles(roles ...security.Role) Routes {
	for i := range rs {
		rs[i].AuthRequire = len(roles) > 0
		rs[i].Roles = roles
	}
	return rs
}
