// +build !integration

package router

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/skillogs-platform/common-lib/xhttp/security"
)

func MyHandler(w http.ResponseWriter, r *http.Request) {

}

func TestNewRoute(t *testing.T) {
	r := NewRoute("GET", "/route", MyHandler)
	assert.Equal(t, r.Name, "MyHandler")
}

func TestRoute_WithAuthRequire(t *testing.T) {
	r := NewRoute("GET", "/route", MyHandler).WithAuthRequire(true)
	assert.Len(t, r.Roles, 5)
	assert.Equal(t, []security.Role{
		security.RoleAdmin,
		security.RoleTrainingEngineer,
		security.RoleTeacher,
		security.RoleStudent,
		security.RoleTutor,
	}, r.Roles)
	r = NewRoute("GET", "/route", MyHandler).WithAuthRequire(false)
	assert.Nil(t, r.Roles)
}

func TestRoute_WithRoles(t *testing.T) {
	r := NewRoute("GET", "/route", MyHandler).WithRoles(security.RoleStudent)
	assert.Len(t, r.Roles, 1)
	assert.Equal(t, []security.Role{security.RoleStudent}, r.Roles)
}

func TestRoute_WithParams(t *testing.T) {
	r := NewRoute("GET", "/route", MyHandler).WithParams(map[string]string{"filter": "filter"})
	assert.Len(t, r.Params, 1)
	assert.Equal(t, "filter", r.Params["filter"])
}

func TestRoutes_WithAuthRequire(t *testing.T) {
	rs := Routes{NewRoute("GET", "/route", MyHandler)}.WithAuthRequire(true)
	assert.Len(t, rs[0].Roles, 5)
	assert.Equal(t, []security.Role{
		security.RoleAdmin,
		security.RoleTrainingEngineer,
		security.RoleTeacher,
		security.RoleStudent,
		security.RoleTutor,
	}, rs[0].Roles)
	rs = Routes{NewRoute("GET", "/route", MyHandler)}.WithAuthRequire(false)
	assert.Nil(t, rs[0].Roles)
}

func TestRoutes_WithRoles(t *testing.T) {
	rs := Routes{NewRoute("GET", "/route", MyHandler)}.WithRoles(security.RoleStudent)
	assert.Len(t, rs[0].Roles, 1)
	assert.Equal(t, []security.Role{security.RoleStudent}, rs[0].Roles)
}
