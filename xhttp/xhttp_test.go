package xhttp

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCORSHandlerAddHeaders(t *testing.T) {
	called := false
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { called = true })

	rw := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "https://localhost/example", nil)
	CORS(handler).ServeHTTP(rw, req)

	assert.True(t, called)
	assert.Equal(t, "*", rw.Header().Get("Access-Control-Allow-Origin"))
	assert.Equal(t, "POST, GET, OPTIONS, PUT, DELETE", rw.Header().Get("Access-Control-Allow-Methods"))
	assert.Equal(t, "Accept, Accept-Language, Content-Type, Authorization", rw.Header().Get("Access-Control-Allow-Headers"))
}

func TestCORSHandlerPreflightOPTIONS(t *testing.T) {
	called := false
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { called = true })

	rw := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodOptions, "https://localhost/example", nil)
	CORS(handler).ServeHTTP(rw, req)

	assert.False(t, called)
	assert.Equal(t, "*", rw.Header().Get("Access-Control-Allow-Origin"))
	assert.Equal(t, "POST, GET, OPTIONS, PUT, DELETE", rw.Header().Get("Access-Control-Allow-Methods"))
	assert.Equal(t, "Accept, Accept-Language, Content-Type, Authorization", rw.Header().Get("Access-Control-Allow-Headers"))
}

func TestRedirectHTTPSDecorator(t *testing.T) {
	called := false
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { called = true })

	rw := httptest.NewRecorder()
	req := httptest.NewRequest("GET", "http://localhost/example", nil)
	req.Header.Add("x-forwarded-proto", "http")
	HTTPS(handler).ServeHTTP(rw, req)

	assert.False(t, called)
	assert.Equal(t, http.StatusPermanentRedirect, rw.Code)
}

func TestHTTPSDecorator(t *testing.T) {
	called := false
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { called = true })

	rw := httptest.NewRecorder()
	req := httptest.NewRequest("GET", "https://localhost/example", nil)
	req.Header.Add("x-forwarded-proto", "https")
	HTTPS(handler).ServeHTTP(rw, req)

	assert.True(t, called)
}

func TestNewHTTPError(t *testing.T) {
	errs := []error{
		errors.New("error1"),
		errors.New("error2"),
		errors.New("error3"),
	}

	err := Error("example", errs...)

	assert.Equal(t, "error1", err.Message)
	assert.Equal(t, "example", err.Domain)
	assert.Len(t, err.Errors, 3)
	assert.Equal(t, "error1", err.Errors[0].Message)
	assert.Equal(t, "error2", err.Errors[1].Message)
	assert.Equal(t, "error3", err.Errors[2].Message)
	assert.Equal(t, "example", err.Errors[0].Domain)
	assert.Equal(t, "example", err.Errors[1].Domain)
	assert.Equal(t, "example", err.Errors[2].Domain)
}

func TestJSONHTTPError(t *testing.T) {
	errs := []error{
		errors.New("error1"),
		errors.New("error2"),
		errors.New("error3"),
	}

	err := Error("example", errs...)

	jsonErr, _ := json.Marshal(err)
	assert.JSONEq(t, `{
		"domain": "example",
		"message": "error1",
		"errors": [
			{
				"domain": "example",
				"message": "error1"
			},{
				"domain": "example",
				"message": "error2"
			},{
				"domain": "example",
				"message": "error3"
			}
		]
	}`, string(jsonErr))
}

func TestJSONResponseWithBody(t *testing.T) {
	type body struct {
		Field1 string `json:"field1"`
		Field2 string `json:"field2"`
	}
	w := httptest.NewRecorder()
	JSONResponse(w, http.StatusOK, body{"var1", "var2"})

	assert.Equal(t, http.StatusOK, w.Code)
	b, _ := ioutil.ReadAll(w.Body)
	assert.JSONEq(t, `{"field1":"var1", "field2":"var2"}`, string(b))
}

func TestJSONResponseWithoutBody(t *testing.T) {
	w := httptest.NewRecorder()
	JSONResponse(w, http.StatusOK, nil)

	assert.Equal(t, http.StatusOK, w.Code)
	b, _ := ioutil.ReadAll(w.Body)
	assert.JSONEq(t, `{}`, string(b))
}

func TestRecover(t *testing.T) {
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { panic("big error") })
	rw := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "https://localhost/example", nil)
	Recover(handler).ServeHTTP(rw, req)
	assert.Equal(t, http.StatusInternalServerError, rw.Code)
}
