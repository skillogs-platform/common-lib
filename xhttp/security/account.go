package security

import "fmt"

type Role string

const (
	RoleAdmin            Role = "ADMIN"
	RoleTrainingEngineer Role = "TRAINING_ENGINEER"
	RoleTeacher          Role = "TEACHER"
	RoleStudent          Role = "STUDENT"
	RoleTutor            Role = "TUTOR"
)

type Lang string

const (
	LangFR Lang = "fr"
	LangEN Lang = "en"
)

func IsValidRole(r Role) bool {
	return r == RoleAdmin ||
		r == RoleTrainingEngineer ||
		r == RoleTeacher ||
		r == RoleStudent ||
		r == RoleTutor
}

// Account represent connected user.
type Account struct {
	ID    string
	Email string
	Name  string
	Role  Role
	Lang  Lang
}

func (a Account) Valid() error {
	if !IsValidRole(a.Role) {
		return fmt.Errorf("invalid role: %s", a.Role)
	}
	if a.Lang != LangFR && a.Lang != LangEN {
		return fmt.Errorf("invalid lang: %s", a.Lang)
	}
	return nil
}

// IsOneOf check if account has one of given role
func (a *Account) IsOneOf(rr ...Role) bool {
	for _, r := range rr {
		if r == a.Role {
			return true
		}
	}
	return false
}
