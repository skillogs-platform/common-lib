// +build !integration

package security

import (
	"crypto/rand"
	"crypto/rsa"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var auth *Auth

func init() {
	k, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		fmt.Println("Fatal error ", err.Error())
		os.Exit(1)
	}
	auth = NewAuthWithPrivate(k)
}

func TestAuth_Verify(t *testing.T) {
	type Case struct {
		token  string
		sheme  string
		status int
	}

	tests := map[string]Case{
		// Http
		"missing_bearer": {"", "https", 401},
		"invalid_token":  {"Bearer " + invalidToken(), "https", 401},
		"valid_admin":    {"Bearer " + validToken(RoleAdmin), "https", 200},
		"valid_student":  {"Bearer " + validToken(RoleStudent), "https", 200},
		"valid_teacher":  {"Bearer " + validToken(RoleTeacher), "https", 200},
		// Webstoket
		"missing_auth_ws":  {"", "wss", 401},
		"valid_admin_ws":   {validToken(RoleAdmin), "wss", 200},
		"invalid_token_ws": {invalidToken(), "wss", 401},
		// NOTE: On laisse passer le verify même sans encryption, car il n'est pas possible de le verifier
		// efficacement et que pour pouvoir developper en local nous avons besoin que ces cas passe.
		"sheme_http": {"Bearer " + validToken(RoleAdmin), "http", 200},
		"sheme_ws":   {validToken(RoleAdmin), "ws", 200},
	}

	for name, tc := range tests {
		tc := tc
		t.Run(name, func(t *testing.T) {
			called := false
			handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { called = true })

			rw := httptest.NewRecorder()
			req := httptest.NewRequest(http.MethodGet, tc.sheme+"://localhost/example", nil)
			switch tc.sheme {
			case "http", "https":
				req.Header.Add("Authorization", tc.token)
			case "ws", "wss":
				q := req.URL.Query()
				q.Add("auth", tc.token)
				req.URL.RawQuery = q.Encode()
				req.Header.Add("Connection", "Upgrade")
				req.Header.Add("Upgrade", "websocket")
			}
			auth.Verify(handler).ServeHTTP(rw, req)

			assert.Equal(t, tc.status, rw.Code)
			assert.Equalf(t, tc.status == 200, called, "If status is 200, handler should be called")
		})
	}
}

func TestAuth_Authorize(t *testing.T) {
	type Case struct {
		token   string
		status  int
		account *Account
	}
	tests := map[string]Case{
		"authorized_account_admin":   {validToken(RoleAdmin), 200, &Account{Role: RoleAdmin}},
		"missing_account_on_context": {validToken(RoleAdmin), 403, nil},
		"authorized_account_teacher": {validToken(RoleTeacher), 200, &Account{Role: RoleTeacher}},
		"unauthorized_account":       {validToken(RoleStudent), 403, &Account{Role: RoleStudent}},
	}

	for name, tc := range tests {
		tc := tc
		t.Run(name, func(t *testing.T) {
			called := false
			handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { called = true })

			rw := httptest.NewRecorder()
			req := httptest.NewRequest(http.MethodGet, "https://localhost/example", nil)
			req.Header.Add("Authorization", tc.token)
			req = WithAuthenticated(req, tc.account)

			auth.Authorize(handler, RoleAdmin, RoleTeacher).ServeHTTP(rw, req)

			assert.Equal(t, tc.status == 200, called)
			assert.Equal(t, tc.status, rw.Code)
		})
	}
}

func TestFailDecode(t *testing.T) {
	type Case struct {
		account  Account
		lifetime time.Duration
		err      string
	}
	tests := map[string]Case{
		"missing_claims": {
			account:  Account{},
			lifetime: time.Hour,
			err:      "missing claims",
		},
		"expired_token": {
			account: Account{
				ID:    "account_id",
				Email: "account_id@skillogs.com",
				Name:  "Account Name",
				Role:  RoleAdmin,
				Lang:  LangEN,
			},
			lifetime: -time.Hour,
			err:      "expired",
		},
		"invalid_role": {
			account: Account{
				ID:    "account_id",
				Email: "account_id@skillogs.com",
				Name:  "Account Name",
				Role:  "some_unknown_role",
			},
			lifetime: time.Hour,
			err:      "invalid role",
		},
		"invalid_lang": {
			account: Account{
				ID:    "account_id",
				Email: "account_id@skillogs.com",
				Name:  "Account Name",
				Role:  RoleAdmin,
				Lang:  "de",
			},
			lifetime: time.Hour,
			err:      "invalid lang",
		},
	}

	for name, tc := range tests {
		tc := tc
		t.Run(name, func(t *testing.T) {
			token, err := auth.Encode(tc.account, tc.lifetime)
			assert.NoError(t, err)
			assert.NotEmpty(t, token)

			_, err = auth.Decode(token)

			assert.Error(t, err)
			assert.Contains(t, err.Error(), tc.err)
		})
	}
}

func TestEncodeToken(t *testing.T) {
	acc := Account{
		ID:    "account_id",
		Email: "account_id@skillogs.com",
		Name:  "Account Name",
		Role:  RoleAdmin,
		Lang:  LangEN,
	}

	token, err := auth.Encode(acc, 10*time.Second)
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
}

func TestDecodeToken(t *testing.T) {
	acc := Account{
		ID:    "account_id",
		Email: "account_id@skillogs.com",
		Name:  "Account Name",
		Role:  RoleAdmin,
		Lang:  LangEN,
	}

	tokenstr, err := auth.Encode(acc, time.Hour)
	assert.NoError(t, err)
	assert.NotEmpty(t, tokenstr)

	decoded, err := auth.Decode(tokenstr)

	assert.NoError(t, err)
	assert.Equal(t, &acc, decoded)

}

func validToken(role Role) string {
	token, err := auth.Encode(Account{ID: "azerty", Email: "test@ŧest.com", Name: "Account Name", Role: role}, time.Hour)
	if err != nil {
		panic(err)
	}
	return token
}

func invalidToken() string {
	token, _ := auth.Encode(Account{}, time.Hour)
	return token
}

func TestAuthenticated(t *testing.T) {
	acc := &Account{ID: "id"}
	req := &http.Request{}

	connected, err := Authenticated(WithAuthenticated(req, acc))

	assert.NoError(t, err)
	assert.Equal(t, acc.ID, connected.ID)
}
