package security

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIsValidRole(t *testing.T) {
	type Test struct {
		role   Role
		result bool
	}
	tests := []Test{
		{Role(""), false},
		{RoleStudent, true},
		{RoleTeacher, true},
		{RoleAdmin, true},
	}
	for _, test := range tests {
		assert.Equal(t, test.result, IsValidRole(test.role))
	}
}

func TestAccount_IsAuthorizedAdmin(t *testing.T) {
	acc := Account{Role: RoleAdmin}
	assert.True(t, acc.IsOneOf(RoleAdmin))
	assert.False(t, acc.IsOneOf(RoleTeacher, RoleStudent, RoleTutor, RoleTrainingEngineer))
}

func TestAccountHasRoles(t *testing.T) {
	acc := Account{Role: RoleTeacher}
	assert.True(t, acc.IsOneOf(RoleAdmin, RoleTeacher))

	acc = Account{Role: RoleStudent}
	assert.False(t, acc.IsOneOf(RoleAdmin, RoleTeacher))
}
