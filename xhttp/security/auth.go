package security

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"crypto/rsa"

	"gitlab.com/skillogs-platform/common-lib/xhttp"
	"gitlab.com/skillogs-platform/common-lib/xhttp/logger"

	"github.com/dgrijalva/jwt-go"
	"github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

// ContextKey is a type that handle custom value in http context
type ContextKey string

// AccountContextKey handle user key in http context
// DEPRECATED: Do no use this anymore, use security.Authenticated
const AccountContextKey ContextKey = "User"

var (
	errMissingBearer            = xhttp.Error("authentication", errors.New("cannot find Bearer in Authorization header"))
	errMissingAuthenticatedUser = xhttp.Error("authentication", errors.New("cannot find authenticated user"))
	errMissingRole              = xhttp.Error("authorization", errors.New("authenticated user has not wanted role"))
)

// Auth service allow to check and authenticate a request
type Auth struct {
	publicKey  *rsa.PublicKey
	privateKey *rsa.PrivateKey
}

// NewAuth create an Auth service to Decode token
func NewAuth(pub *rsa.PublicKey) *Auth {
	return &Auth{publicKey: pub}
}

// NewAuthWithPrivate create an Auth service with a private key to Encode token
func NewAuthWithPrivate(private *rsa.PrivateKey) *Auth {
	return &Auth{publicKey: private.Public().(*rsa.PublicKey), privateKey: private}
}

// Verify is a middleware that identify an account from jwt token
func (a *Auth) Verify(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenstr := ""
		switch {
		case isWebsocket(r):
			tokenstr = r.URL.Query().Get("auth")
			if tokenstr == "" {
				logrus.Error("Cannot find Bearer in query param for websoket")
				xhttp.JSONResponse(w, http.StatusUnauthorized, errMissingBearer)
				return
			}
		default:
			auth := r.Header.Get("Authorization")
			parts := strings.Split(auth, " ")
			if len(auth) == 0 || parts[0] != "Bearer" {
				logrus.Info("Cannot find Bearer in Authorization header")
				xhttp.JSONResponse(w, http.StatusUnauthorized, errMissingBearer)
				return
			}
			tokenstr = parts[1]
		}

		acc, err := a.Decode(tokenstr)
		if err != nil {
			logrus.WithError(err).Info("Authentication fail (couldn't decode claims)")
			xhttp.JSONResponse(w, http.StatusUnauthorized, xhttp.Error("authentication", err))
			return
		}
		r = WithAuthenticated(r, acc)
		r = logger.WithLoggerEntry(r, logrus.
			WithField("user", acc.ID).
			WithField("request_id", r.Header.Get("SK-REQUEST-ID")))

		logrus.Debugf("Authenticated user %s", acc.Email)
		next.ServeHTTP(w, r)
	})
}

func isWebsocket(r *http.Request) bool {
	return strings.Contains(r.Header.Get("Connection"), "Upgrade") &&
		r.Header.Get("Upgrade") == "websocket"
}

// Authorize is a middleware that check if authenticated account has required role
func (a *Auth) Authorize(next http.Handler, roles ...Role) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		acc, err := Authenticated(r)
		if err != nil {
			logrus.Info("Cannot find authenticated user in request")
			xhttp.JSONResponse(w, http.StatusForbidden, errMissingAuthenticatedUser)
			return
		}
		if !acc.IsOneOf(roles...) {
			logrus.Info("Connected user has not wanted role")
			xhttp.JSONResponse(w, http.StatusForbidden, errMissingRole)
			return
		}
		logrus.Debugf("Authorization success %s %s", acc.Email, acc.Role)
		next.ServeHTTP(w, r)
	})
}

// Encode a token string from an account
func (a Auth) Encode(acc Account, lifetime time.Duration) (string, error) {
	claims := make(jwt.MapClaims)
	claims["sub"] = acc.ID
	claims["name"] = acc.Name
	claims["email"] = acc.Email
	claims["role"] = acc.Role
	claims["lang"] = acc.Lang
	if acc.Lang == "" {
		claims["lang"] = LangFR
	}
	claims["exp"] = time.Now().Add(lifetime).Unix()
	claims["iat"] = time.Now().Unix()

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	return token.SignedString(a.privateKey)
}

func (a Auth) parseFunc(t *jwt.Token) (interface{}, error) {
	if _, ok := t.Method.(*jwt.SigningMethodRSA); !ok {
		return nil, fmt.Errorf("unexpected signing method: %v", t.Header["alg"])
	}
	return a.publicKey, nil
}

// Decode transform a token as an Account
func (a Auth) Decode(tokenstr string) (*Account, error) {
	token, err := jwt.Parse(tokenstr, a.parseFunc)
	if err != nil || !token.Valid {
		return nil, fmt.Errorf("invalid token: %s", err)
	}
	// NOTE: dans la suite de l'implementation, nous ignorons la verification
	// des types car nous controllons Encode et Decode
	claims, _ := token.Claims.(jwt.MapClaims)
	if err := claims.Valid(); err != nil {
		return nil, fmt.Errorf("token's claims are not valid: %s", err)
	}

	sub, _ := claims["sub"].(string)
	email, _ := claims["email"].(string)
	name, _ := claims["name"].(string)
	role, _ := claims["role"].(string)
	lang, _ := claims["lang"].(string)
	if lang == "" {
		lang = string(LangFR)
	}
	if sub == "" || email == "" || name == "" || role == "" {
		return nil, errors.New("missing claims sub or email or name or role")
	}
	acc := Account{
		ID:    sub,
		Email: email,
		Name:  name,
		Lang:  Lang(lang),
		Role:  Role(role),
	}

	return &acc, acc.Valid()
}

// WithAuthenticated add Account to http.Request
func WithAuthenticated(r *http.Request, acc *Account) *http.Request {
	return r.WithContext(context.WithValue(r.Context(), AccountContextKey, acc))
}

// Authenticated extract authenticated account from http.Request context
// If there is no error, the returned account will not be nil
func Authenticated(r *http.Request) (*Account, error) {
	acc, ok := r.Context().Value(AccountContextKey).(*Account)
	if !ok {
		return nil, errors.New("cannot find authenticated account in http.Request context")
	}
	if acc == nil {
		return nil, errors.New("no authenticated account in http.Request context")
	}
	return acc, nil
}
