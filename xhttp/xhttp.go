package xhttp

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/sirupsen/logrus"
)

// Recover is a decorator to an http.Handler that capture global panic, log it and return JSON error.
func Recover(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if r := recover(); r != nil {
				logrus.Errorf("Panic: %v", r)
				JSONResponse(w, 500, HTTPError{Message: r.(string), Domain: "panic"})
			}
		}()
		next.ServeHTTP(w, r)
	})
}

// CORS is a decorator to an http.Handler that enable CORS origin request on OPTIONS header.
func CORS(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Accept-Language, Content-Type, Authorization")
		if r.Method == http.MethodOptions {
			return
		}
		next.ServeHTTP(w, r)
	})
}

// HTTPS is a decorator to an http.Handler that redirect to https
func HTTPS(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		proto := r.Header.Get("x-forwarded-proto")
		if strings.ToLower(proto) == "http" {
			logrus.WithFields(logrus.Fields{
				"from": "http://" + r.Host,
				"to":   "https://" + r.Host,
			}).Info("Redirect request")
			http.Redirect(w, r, fmt.Sprintf("https://%s%s", r.Host, r.URL), http.StatusPermanentRedirect)
			return
		}
		next.ServeHTTP(w, r)
	})
}

// HTTPError is a json representation of a server error
type HTTPError struct {
	Message string      `json:"message"`
	Domain  string      `json:"domain"`
	Errors  []HTTPError `json:"errors,omitempty"`
}

// Error return an HTTP error that can be encode to json.
func Error(domain string, errs ...error) HTTPError {
	httpErrors := make([]HTTPError, len(errs))
	for i, e := range errs {
		httpErrors[i] = HTTPError{Message: e.Error(), Domain: domain}
	}
	return HTTPError{
		Message: errs[0].Error(),
		Domain:  domain,
		Errors:  httpErrors,
	}
}

// EmptyResponse is a constant that represent an empty response.
var EmptyResponse interface{} = struct{}{}

// JSONResponse write body to ResponseWriter in json format
func JSONResponse(w http.ResponseWriter, httpStatus int, body interface{}) {
	if body == nil {
		body = EmptyResponse
	}
	res, err := json.Marshal(body)
	if err != nil {
		JSONResponse(w, http.StatusInternalServerError, Error("application", err))
		return
	}

	w.Header().Add("Content-Type", "application/json;charset=utf-8")
	w.WriteHeader(httpStatus)
	_, err = w.Write(res)
	if err != nil {
		logrus.WithError(err).Errorf("Cannot write response")
	}
}
