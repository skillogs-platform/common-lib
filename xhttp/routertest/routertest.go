package routertest

import (
	"gitlab.com/skillogs-platform/common-lib/xhttp/router"
	"gitlab.com/skillogs-platform/common-lib/xhttp/security"

	"net/http"
	"net/http/httptest"
	"strings"
)

type R struct {
	c router.Routable
	r *router.Router
}

func New(ctrl router.Routable) *R {
	auth := &AuthStub{}
	r := router.NewRouter(auth)
	r.HandleRoutable("/test", ctrl)
	return &R{r: r, c: ctrl}
}

func (tr *R) WithAuth(auth router.Auth) *R {
	r := router.NewRouter(auth)
	r.HandleRoutable("/test", tr.c)
	return &R{r: r, c: tr.c}
}

func (tr R) Get(endpoint string, opts ...RequestOption) *httptest.ResponseRecorder {
	req, _ := http.NewRequest("GET", "/test"+endpoint, nil)
	return tr.Do(req, opts...)
}

func (tr R) Head(endpoint string, opts ...RequestOption) *httptest.ResponseRecorder {
	req, _ := http.NewRequest("HEAD", "/test"+endpoint, nil)
	return tr.Do(req, opts...)
}

func (tr R) Delete(endpoint string, opts ...RequestOption) *httptest.ResponseRecorder {
	req, _ := http.NewRequest("DELETE", "/test"+endpoint, nil)
	return tr.Do(req, opts...)
}

func (tr R) Put(endpoint string, body string, opts ...RequestOption) *httptest.ResponseRecorder {
	req, _ := http.NewRequest("PUT", "/test"+endpoint, strings.NewReader(body))
	return tr.Do(req, opts...)
}

func (tr R) Post(endpoint string, body string, opts ...RequestOption) *httptest.ResponseRecorder {
	req, _ := http.NewRequest("POST", "/test"+endpoint, strings.NewReader(body))
	return tr.Do(req, opts...)
}

// Do perform http.Request with given RequestOptions
func (tr R) Do(req *http.Request, opts ...RequestOption) *httptest.ResponseRecorder {
	for _, o := range opts {
		req = o(req)
	}
	rr := httptest.NewRecorder()
	tr.r.ServeHTTP(rr, req)
	return rr
}

// RequestOption allows to modify http.Request before sending it.
type RequestOption func(*http.Request) *http.Request

// WithHeader add key and value in req.Header
func WithHeader(k, v string) RequestOption {
	return RequestOption(func(req *http.Request) *http.Request {
		req.Header.Add(k, v)
		return req
	})
}

// WithUserContext add an account to the http.Request context.
func WithUserContext(acc *security.Account) RequestOption {
	return RequestOption(func(req *http.Request) *http.Request {
		return security.WithAuthenticated(req, acc)
	})
}

type AuthStub struct{}

func (a AuthStub) Verify(next http.Handler) http.Handler                            { return next }
func (a AuthStub) Authorize(next http.Handler, roles ...security.Role) http.Handler { return next }
