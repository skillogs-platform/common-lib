package logger

import (
	"context"
	"net/http"

	"github.com/sirupsen/logrus"
)

type ContextKey string

// LoggerContextKey add userID key in http context
const LoggerContextKey ContextKey = "logger"

// WithLoggerEntry adds logger entry to http.Request
func WithLoggerEntry(r *http.Request, entry *logrus.Entry) *http.Request {
	return r.WithContext(context.WithValue(r.Context(), LoggerContextKey, entry))
}

// Entry extracts logger entry from http.Request context
// If there is an error, the returned logger entry will be the Standard Logger
func Entry(r *http.Request) *logrus.Entry {
	entry, ok := r.Context().Value(LoggerContextKey).(*logrus.Entry)
	if !ok || entry == nil {
		logrus.Warn("Cannot find logger entry in http.Request context")
		return logrus.NewEntry(logrus.StandardLogger())
	}
	return entry
}
