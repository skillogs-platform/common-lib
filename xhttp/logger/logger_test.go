// +build !integration,!e2e

package logger

import (
	"net/http"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestLoggerEntry(t *testing.T) {
	req := &http.Request{}
	entry := logrus.WithField("user", "toto_id")

	log := Entry(WithLoggerEntry(req, entry))

	assert.Equal(t, "toto_id", log.Data["user"])
}

func TestLoggerEntry_WithoutUserField(t *testing.T) {
	req := &http.Request{}

	log := Entry(req)

	assert.Nil(t, log.Data["user"])
}
