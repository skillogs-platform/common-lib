package page

import (
	"net/url"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreatePage(t *testing.T) {
	ss := []string{"1", "2", "3"}
	page := NewPage(ss, 3, 0)

	assert.Len(t, ss, page.Count())
	assert.Equal(t, ss, page.Result())
	assert.Equal(t, 0, page.Number())
}

func TestMapPageResult(t *testing.T) {
	ss := []string{"1", "2", "3"}
	page := NewPage(ss, 3, 0)
	mapper := func(i interface{}) interface{} {
		ss, _ := i.([]string)
		dd := make([]int, len(ss))
		for i, se := range ss {
			dd[i], _ = strconv.Atoi(se)
		}
		return dd
	}

	page.Map(mapper)

	assert.Equal(t, []int{1, 2, 3}, page.Result())
}

func TestMapPageWithError(t *testing.T) {
	ss := []string{"1", "2", "something_bad"}
	page := NewPage(ss, 3, 0)
	mapper := func(i interface{}) (interface{}, error) {
		ss, _ := i.([]string)
		dd := make([]int, len(ss))
		for i, se := range ss {
			r, e := strconv.Atoi(se)
			if e != nil {
				return nil, e
			}
			dd[i] = r
		}
		return dd, nil
	}

	_, err := page.MapWithError(mapper)

	assert.Error(t, err)
}

func TestShouldSkipAndLimit(t *testing.T) {
	r := Request{
		Page: 0,
		Size: 10,
	}
	assert.Equal(t, 0, r.Skip())
	assert.Equal(t, 10, r.Limit())

	r = Request{
		Page: 1,
		Size: 10,
	}
	assert.Equal(t, 10, r.Skip())
	assert.Equal(t, 10, r.Limit())

	r = Request{
		Page: 2,
		Size: 12,
	}
	assert.Equal(t, 24, r.Skip())
	assert.Equal(t, 12, r.Limit())
}

func TestCreateRequestFromURLValues(t *testing.T) {
	v, _ := url.ParseQuery("page_size=20&page_number=1&sort=field1:asc,field2:desc")

	req := NewRequest(v)

	assert.Equal(t, 20, req.Size)
	assert.Equal(t, 1, req.Page)
	assert.Equal(t, "field1", req.Sort[0].Field)
	assert.Equal(t, ASC, req.Sort[0].Direction)
	assert.Equal(t, "field2", req.Sort[1].Field)
	assert.Equal(t, DESC, req.Sort[1].Direction)
}

func TestCreateRequestWithMissingURLValues(t *testing.T) {
	v, _ := url.ParseQuery("")

	req := NewRequest(v)

	assert.Equal(t, 20, req.Size)
	assert.Equal(t, 0, req.Page)
	assert.Equal(t, "id", req.Sort[0].Field)
	assert.Equal(t, DESC, req.Sort[0].Direction)
}
