package page

import (
	"strconv"
	"strings"
)

func sortFrom(s string) Sort {
	ss := strings.Split(s, ",")
	sort := make(Sort, len(ss))
	for i, sv := range ss {
		sort[i] = orderFrom(sv)
	}
	return sort
}

func orderFrom(s string) Order {
	oo := strings.Split(s, ":")

	return Order{
		Field:     oo[0],
		Direction: directionFrom(oo[1]),
	}
}

func directionFrom(s string) Direction {
	s = strings.ToUpper(s)
	switch s {
	case "ASC":
		return ASC
	case "DESC":
		return DESC
	default:
		return DESC
	}
}

func parseInt(s string, fallback int) int {
	i, err := strconv.Atoi(s)
	if err != nil {
		i = fallback
	}
	return i

}
