// Package page allows to create request and response to get a page.
package page

const (
	ASC Direction = iota
	DESC

	DefaultPageSize   int = 20
	DefaultPageNumber int = 0
)

// DefaultPageSort define base value if page request missing sort value
// nolint:gochecknoglobals
var DefaultPageSort Sort = []Order{{Field: "id", Direction: DESC}}

// DefaultRequest is a Request with all default enabled
// nolint:gochecknoglobals
var DefaultRequest = NewRequest(map[string][]string{})

// Page handle response with total count
type Page struct {
	R interface{} `json:"result"`
	C int         `json:"count"`
	N int         `json:"number"`
}

func (p Page) Result() interface{} { return p.R }
func (p Page) Count() int          { return p.C }
func (p Page) Number() int         { return p.N }

// Map allows to modify result with the given mapper.
// This is useful to create DTO from original result.
func (p *Page) Map(mapper func(interface{}) interface{}) *Page {
	p.R = mapper(p.R)
	return p
}

// MapWithError allows to modify result with the given mapper and manage error.
// This is useful to create DTO from original result.
func (p *Page) MapWithError(mapper func(interface{}) (interface{}, error)) (*Page, error) {
	var err error
	p.R, err = mapper(p.R)
	if err != nil {
		return p, err
	}
	return p, nil
}

// NewPage create a Page for given slice, count and page number
func NewPage(slice interface{}, count, number int) *Page {
	return &Page{
		R: slice,
		C: count,
		N: number,
	}
}

// Request a Page
type Request struct {
	// Page is zero-based page index.
	Page int
	// Number of row we want to be return
	Size int
	Sort Sort
}

// NewRequest create a Request. A url.Values can be passed.
// As url.Values, this is the first value of map that is used.
//
// It take his value from keys :
// - Page : "page_number"
// - Size : "page_size"
// - Sort : "sort"
//
func NewRequest(m map[string][]string) (req Request) {
	req.Page = DefaultPageNumber
	req.Size = DefaultPageSize
	req.Sort = DefaultPageSort
	if v, exist := m["page_number"]; exist {
		req.Page = parseInt(v[0], 0)
	}
	if v, exist := m["page_size"]; exist {
		req.Size = parseInt(v[0], 20)
	}
	if v, exist := m["sort"]; exist {
		req.Sort = sortFrom(v[0])
	}
	return req
}

// Skip indicate how many element that should be skipped with page number and size.
func (r Request) Skip() int { return r.Page * r.Size }

// Limit indicate how many resutls that we want to be returned.
func (r Request) Limit() int { return r.Size }

// Sort is an order slice.
//
// It allows to use multiple order as :
// 1 - birthday : ASC
// 2 - lastName : DESC
type Sort []Order

// Order allows set a direction for a field
type Order struct {
	Field     string
	Direction Direction
}

// Direction is an enumeration for sort directions
type Direction int
