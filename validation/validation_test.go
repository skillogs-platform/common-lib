package validation

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewErr(t *testing.T) {
	err := NewError("email", CodeMissing)
	assert.Equal(t, "email.missing", err.Error())
}

func TestAddErrorIfEmpty(t *testing.T) {
	v := New()
	v.AddIfEmpty("", "field")

	if assert.True(t, v.HasError()) {
		assert.Equal(t, "field.missing", v.Errors()[0].Error())
	}
}

func TestAddError(t *testing.T) {
	v := New()
	v.AddIf(true, NewError("field", CodeFormat))

	if assert.True(t, v.HasError()) {
		assert.Equal(t, "field.format", v.Errors()[0].Error())
	}
}

func TestMultipleErrorString(t *testing.T) {
	v := New()
	v.AddIf(true, NewError("field", CodeFormat))
	v.AddIf(true, NewError("field", CodeLacking))

	if assert.True(t, v.HasError()) {
		assert.Equal(t, "field.format - field.lacking", v.Error())
	}
}
