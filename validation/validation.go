package validation

import (
	"strings"
)

type Code string

const (
	CodeUnknow      Code = "unknow"
	CodeFormat      Code = "format"
	CodeMissing     Code = "missing"
	CodeBeforeBegin Code = "beforeBegin"
	CodeImmutable   Code = "immutable"
	CodeLacking     Code = "lacking"
	CodeForbidden   Code = "forbidden"
	CodeExists      Code = "exists"
)

// Error handle a field error associated with a code
type Error struct {
	field string
	code  Code
}

// NewError returns an error that formats as the given text.
func NewError(field string, code Code) error {
	return &Error{field, code}
}

func (e *Error) Error() string {
	return e.field + "." + string(e.code)
}

// Validation is a builder that handle errors added by its methods
type Validation struct {
	errors []error
}

// New create a new Validation
func New() Validation {
	return Validation{make([]error, 0)}
}

// HasError return true if Validation contains more than one error
func (v Validation) HasError() bool { return len(v.errors) > 0 }

// Errors return all errors added
func (v Validation) Errors() []error { return v.errors }

// Error return all errors added formated
func (v Validation) Error() string { return v.String() }

// AddIf adds errors if predicate is true
func (v *Validation) AddIf(predicate bool, err error) bool {
	if predicate {
		v.errors = append(v.errors, err)
	}
	return predicate
}

// AddIfEmpty adds error missing if given string is empty
func (v *Validation) AddIfEmpty(str string, field string) bool {
	return v.AddIf(str == "", NewError(field, CodeMissing))
}

// String return all error in formated string
func (v *Validation) String() string {
	ss := make([]string, len(v.errors))
	for i, err := range v.errors {
		ss[i] = err.Error()
	}
	return strings.Join(ss, " - ")
}
