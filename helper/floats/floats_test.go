package floats

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRound(t *testing.T) {
	assert.Equal(t, 3, Round(3.14159265359))
}

func TestToFixed(t *testing.T) {
	assert.Equal(t, 3.14, ToFixed(3.14159265359, 2))
	assert.Equal(t, 3.142, ToFixed(3.14159265359, 3))
	assert.Equal(t, 3.1416, ToFixed(3.14159265359, 4))
}
